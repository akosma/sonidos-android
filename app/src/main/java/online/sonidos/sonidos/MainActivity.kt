package online.sonidos.sonidos

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import io.swagger.client.api.SongsApi
import io.swagger.client.model.Song
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.FileOutputStream
import java.io.IOException
import java.io.File
import android.media.MediaPlayer


class MainActivity : AppCompatActivity() {

    private inner class SongHolder internal constructor(v: View)
        : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var song: Song? = null
        private val titleTextView: TextView

        init {
            v.setOnClickListener(this)
            titleTextView = v.findViewById(R.id.name_text_view)
        }

        internal fun bind(song: Song) {
            this.song = song
            titleTextView.text = song.displayFilename
        }

        override fun onClick(view: View) {
            select(song!!)
        }
    }

    private inner class SongsAdapter internal constructor(private val data: List<Song>)
        : RecyclerView.Adapter<SongHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongHolder {
            val inflater = LayoutInflater.from(this@MainActivity)
            val view = inflater.inflate(R.layout.list_item_song, parent, false)
            return SongHolder(view)
        }

        override fun onBindViewHolder(holder: SongHolder, position: Int) {
            val song = data[position]
            holder.bind(song)
        }

        override fun getItemCount() = data.size
    }

    private val invoker = SongsApi()
    private lateinit var songs : List<Song>
    private var player : MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView.layoutManager = LinearLayoutManager(this)
        invoker.basePath = "https://sonidos-api.eu.ngrok.io"
    }

    override fun onResume() {
        super.onResume()
        val runnable = Runnable {
            try {
                songs = invoker.allSongs()
                runOnUiThread {
                    updateList()
                }
            }
            catch(e: Exception) {
                Log.e("Network", "Network connection failed: ${e.message}")
            }
        }
        val thread = Thread(runnable)
        thread.start()
    }

    private fun updateList() {
        recyclerView.adapter = SongsAdapter(songs)
    }

    private fun select(song: Song) {
        val runnable = Runnable {
            val path = "${filesDir}/${song.displayFilename}"
            val file = File(path)
            if (!file.exists()) {
                download(song, path)
            }
            try {
                play(song, path)
            }
            catch (e: Exception) {
                file.delete()
                download(song, path)
            }
            play(song, path)
        }
        val thread = Thread(runnable)
        thread.start()
    }

    private fun download(song: Song, path: String) {
        runOnUiThread {
            Toast.makeText(this@MainActivity, "Downloading ${song.displayFilename}", Toast.LENGTH_SHORT).show()
        }

        val client = OkHttpClient()
        val request = Request.Builder().url("https://sonidos-api.eu.ngrok.io/api/v1/songs/${song.id}").build()
        val response = client.newCall(request).execute()
        if (!response.isSuccessful) {
            throw IOException("Failed to download file: $response")
        }
        val fos = FileOutputStream(path)
        fos.write(response.body()?.bytes())
        fos.close()

        runOnUiThread {
            Toast.makeText(this@MainActivity, "File ${song.displayFilename} downloaded", Toast.LENGTH_SHORT).show()
        }
    }

    private fun isPlaying() = if (player != null) player?.isPlaying!! else false

    private fun play(song: Song, path: String) {
        runOnUiThread {
            if (isPlaying()) {
                player?.stop()
            }
            player = MediaPlayer()
            player?.setDataSource(path)
            player?.prepare()
            player?.start()
        }
    }
}
