#!/usr/bin/env sh

# Download Swagger Codegen if not available
if [ ! -f swagger-codegen-cli.jar ]; then
    echo "Downloading Swagger Codegen"
    wget http://central.maven.org/maven2/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O swagger-codegen-cli.jar
fi

# Generate the code for the Android app
# ATTENTION: this requires Java 8 maximum;
# it does not work with Java 10
java -jar swagger-codegen-cli.jar generate --input-spec https://sonidos-api.eu.ngrok.io/swagger/v1/swagger.json --output sonidos --lang android

# Make the gradlew script executable
cd sonidos
chmod +x gradlew

# Change the Gradle version in the properties file
# https://askubuntu.com/a/20416
sed -i 's/2.6/3.3/g' gradle/wrapper/gradle-wrapper.properties

# Build the code
ANDROID_HOME=/home/akosma/Android/Sdk ./gradlew build
cd ..

# Update the jar in the libs folder
rm ../app/libs/sonidos-release-1.0.0.jar
cp sonidos/build/outputs/jar/sonidos-release-1.0.0.jar ../app/libs

# Erase the previous generated code
rm -rf sonidos

